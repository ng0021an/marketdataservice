package marketdataservice.reader;

import marketdataservice.main.DataService;
import marketdataservice.config.AppConfig;
import marketdataservice.config.MarketDataConfigProperties;
import marketdataservice.model.MarketDataModel;
import marketdataservice.repository.PriceDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
@Order(value=30)
public class MarketDataReader implements DataService {

    @Autowired
    private MarketDataConfigProperties marketDataConfigProperties;

    @Autowired
    @AppConfig.ReaderExecutor
    private ScheduledExecutorService executorService;

    @Autowired
    private MarketDataModel marketDataModel;

    @Autowired
    private PriceDataRepository priceDataRepository;

    @Override
    public void start() {
        marketDataConfigProperties.getSymbolConfigs().forEach(symbolConfig ->
                executorService.scheduleAtFixedRate(new DataPersister(symbolConfig.getSymbol(), marketDataModel, priceDataRepository), 0, symbolConfig.getFrequencyInSeconds(), TimeUnit.SECONDS)
        );
    }
}
