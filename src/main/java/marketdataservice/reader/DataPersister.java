package marketdataservice.reader;

import lombok.extern.log4j.Log4j2;
import marketdataservice.model.MarketDataModel;
import marketdataservice.model.PriceData;
import marketdataservice.repository.PriceDataRepository;

import java.util.Optional;

@Log4j2
public class DataPersister implements Runnable {

    private final String symbol;
    private final MarketDataModel marketDataModel;
    private final PriceDataRepository priceDataRepository;

    public DataPersister(String symbol, MarketDataModel marketDataModel, PriceDataRepository priceDataRepository) {
        this.symbol = symbol;
        this.marketDataModel = marketDataModel;
        this.priceDataRepository = priceDataRepository;
    }

    @Override
    public void run() {
        Optional<PriceData> priceData = marketDataModel.getPriceData(symbol);
        try {
            if (priceData.isPresent()) {
                priceDataRepository.save(priceData.get());
                log.info(String.format("%s persisted", symbol));
            }
        }
        catch (Exception e) {
            log.error("Error persisting price data " + priceData, e);
        }
    }
}
