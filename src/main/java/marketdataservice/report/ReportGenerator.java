package marketdataservice.report;

import lombok.extern.log4j.Log4j2;
import marketdataservice.main.DataService;
import marketdataservice.config.AppConfig;
import marketdataservice.config.MarketDataConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
@Order(value=40)
@Log4j2
//Since there was no sample report attached, I assumed my own format of the report
public class ReportGenerator implements Runnable, DataService {

    @Autowired
    @AppConfig.ReportExecutor
    protected ScheduledExecutorService executorService;

    @Autowired
    protected MarketDataConfigProperties marketDataConfigProperties;

    @Autowired
    protected ReportGeneratorQueryPlan reportGeneratorQueryPlan;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void start() {
        executorService.schedule(this, marketDataConfigProperties.getReportGenerationDelayInSeconds(), TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        try {
            log.info("****************REPORT****************");
            reportGeneratorQueryPlan.generateReportData().forEach(reportSymbolData -> log.info(String.format("%s - 2nd highest: %s, average: %s", reportSymbolData.getSymbol(), format(reportSymbolData.getKthHighestPrice()), format(reportSymbolData.getAveragePrice()))));
            SpringApplication.exit(applicationContext, () -> 0);
        }
        catch (Exception e) {
            log.error("Error generating report", e);
            SpringApplication.exit(applicationContext, () -> -1);
        }
    }

    private String format(Number number) {
        return number == null ? null : new DecimalFormat("#,##0.00").format(number);
    }
}
