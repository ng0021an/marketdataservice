package marketdataservice.report;

import lombok.Data;
import marketdataservice.config.MarketDataConfigProperties;
import marketdataservice.repository.PriceDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReportGeneratorQueryPlan {

    @Autowired
    private MarketDataConfigProperties marketDataConfigProperties;

    @Autowired
    private PriceDataRepository priceDataRepository;

    public List<ReportSymbolData> generateReportData() {
        return marketDataConfigProperties.getSymbolConfigs().stream().map(MarketDataConfigProperties.SymbolConfig::getSymbol).map(
                symbol -> new ReportSymbolData(symbol,
                        priceDataRepository.getKthHighestPriceForSymbol(symbol, marketDataConfigProperties.getKHighestPrice()),
                        priceDataRepository.getAveragePriceForSymbol(symbol, marketDataConfigProperties.getAveragePricePeriodInSeconds()))
        ).collect(Collectors.toList());
    }

    @Data
    public static class ReportSymbolData {
        private final String symbol;
        private final BigDecimal kthHighestPrice;
        private final Double averagePrice;
    }
}
