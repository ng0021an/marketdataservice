package marketdataservice.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Getter
@NoArgsConstructor
public class PriceData {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(nullable=false)
    private String id;

    @Column(nullable=false)
    private ZonedDateTime timeStamp;

    @Column(nullable=false)
    private String symbol;

    @Column(nullable=false, precision=7, scale=2)
    private BigDecimal price;

    public PriceData(ZonedDateTime timeStamp, String symbol, BigDecimal price) {
        this.timeStamp = timeStamp;
        this.symbol = symbol;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PriceData))
            return false;

        PriceData priceData = (PriceData) o;
        return timeStamp.equals(priceData.getTimeStamp())
                && symbol.equals(priceData.getSymbol())
                && price.doubleValue() == priceData.getPrice().doubleValue();
    }

}
