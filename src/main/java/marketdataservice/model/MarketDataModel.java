package marketdataservice.model;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Log4j2
public class MarketDataModel {

    //Use concurrenthashmap since the data is accessed by many thread
    private final Map<String, PriceData> priceDataMap = new ConcurrentHashMap<>();

    public void update(List<PriceData> priceDataList) {
        priceDataList.forEach(priceData -> {
            priceDataMap.put(priceData.getSymbol(), priceData);
            log.info(String.format("%s:%s", priceData.getSymbol(), priceData.getPrice()));
        });
    }

    public Optional<PriceData> getPriceData(String symbol) {
        return Optional.ofNullable(priceDataMap.get(symbol));
    }
}
