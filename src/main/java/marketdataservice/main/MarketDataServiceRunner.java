package marketdataservice.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class MarketDataServiceRunner {

    @Autowired
    private List<DataService> dataServices;

    @PostConstruct
    public void startServices() {
        dataServices.forEach(dataService -> dataService.start());
    }
}
