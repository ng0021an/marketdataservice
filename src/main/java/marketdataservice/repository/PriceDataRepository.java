package marketdataservice.repository;

import marketdataservice.model.PriceData;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Repository
@Transactional
public class PriceDataRepository {

    private static final String PRICE_COL = "price";
    private static final String SYMBOL_COL = "symbol";
    private static final String TIME_STAMP_COL = "timeStamp";

    @Autowired
    private SessionFactory sessionFactory;

    public void save(PriceData priceData) {
        Session session = sessionFactory.getCurrentSession();
        session.save(priceData);
    }

    public List<PriceData> getAll() {
        return sessionFactory.getCurrentSession().createCriteria(PriceData.class).list();
    }

    public void clear() {
        sessionFactory.getCurrentSession().createQuery("DELETE FROM PriceData").executeUpdate();
    }

    //Here we don't use sort and get by row id since that method is subject to db implementation
    //The logic here will get kth largest entry, not value, meaning if price is 5, 5, 5, 4, 3, 2, the 2nd
    //highest price will be 5 instead of 4.
    //The method used here is for each entry A, count the number of entries strictly greater than A (called m)
    //and number of entries greater than or equal to A (called n)
    //if (m < k <= n) then that entry will be the kth largest value. Note that there will be multiple of
    //such entries, all have the same price so we could pick any one and get the price
    public BigDecimal getKthHighestPriceForSymbol(String symbol, long k) {
        Session session = sessionFactory.getCurrentSession();

        String p1 = "p1", p2 = "p2", p3 = "p3";
        DetachedCriteria striclyGreaterCriteria = DetachedCriteria.forClass(PriceData.class, p2)
                .add(Restrictions.eqProperty(SYMBOL_COL, p1 + "." + SYMBOL_COL))
                .add(Restrictions.gtProperty(PRICE_COL, p1 + "." + PRICE_COL))
                .setProjection(Projections.rowCount());

        DetachedCriteria greaterOrEqualCriteria = DetachedCriteria.forClass(PriceData.class, p3)
                .add(Restrictions.eqProperty(SYMBOL_COL, p1 + "." + SYMBOL_COL))
                .add(Restrictions.geProperty(PRICE_COL, p1 + "." + PRICE_COL))
                .setProjection(Projections.rowCount());

        Criteria outerCriteria = session.createCriteria(PriceData.class, p1)
                .add(Restrictions.eq(SYMBOL_COL, symbol))
                .add(Subqueries.gt(k, striclyGreaterCriteria))
                .add(Subqueries.le(k, greaterOrEqualCriteria));

        List result = outerCriteria.list();
        return result.isEmpty() ? null : ((PriceData) result.get(0)).getPrice();
    }

    public Double getAveragePriceForSymbol(String symbol, int periodInSeconds) {
        Session session = sessionFactory.getCurrentSession();

        Object result = session.createCriteria(PriceData.class)
                .add(Restrictions.eq(SYMBOL_COL, symbol))
                //add 1 to the period to include the price at the start of the period
                .add(Restrictions.ge(TIME_STAMP_COL, ZonedDateTime.now().minus(periodInSeconds + 1, ChronoUnit.SECONDS)))
                .setProjection(Projections.avg(PRICE_COL))
                .uniqueResult();
        return result == null ? null : (Double) result;
    }

}
