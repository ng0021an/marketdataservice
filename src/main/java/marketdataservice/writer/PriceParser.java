package marketdataservice.writer;

import marketdataservice.model.PriceData;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Component
public class PriceParser {

    public PriceData parsePriceData(String line, ZonedDateTime time) throws RuntimeException {
        String[] tokens = line.split(":");

        if (tokens.length < 2)
            throw new RuntimeException("Invalid data format");

        return new PriceData(time, tokens[0], new BigDecimal(tokens[1]));
    }
}
