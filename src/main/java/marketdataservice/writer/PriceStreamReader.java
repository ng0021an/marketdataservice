package marketdataservice.writer;

import lombok.extern.log4j.Log4j2;
import marketdataservice.config.MarketDataConfigProperties;
import marketdataservice.main.DataService;
import marketdataservice.model.PriceData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Order(value=10)
@Log4j2
public class PriceStreamReader implements DataService {

    @Autowired
    private MarketDataConfigProperties marketDataConfigProperties;

    @Autowired
    private PriceParser priceParser;

    @Autowired
    private FileFactory fileFactory;

    private BufferedReader bufferedReader;

    @Override
    public void start() {
        String inputFile = marketDataConfigProperties.getInputFile();
        try {
            bufferedReader = new BufferedReader(new FileReader(fileFactory.readFile(inputFile)));
        }
        catch (Exception e) {
            log.error("Error reading file " + inputFile, e);
        }
    }

    public List<PriceData> readNextPriceGroup() throws IOException {
        List<PriceData> priceGroup = new ArrayList<>();
        for (int i = 0; i < marketDataConfigProperties.getSymbolConfigs().size(); i++) {
            String line = this.bufferedReader.readLine();
            if (line != null) {
                PriceData priceData = this.priceParser.parsePriceData(line, ZonedDateTime.now());
                priceGroup.add(priceData);
            }
            else {
                bufferedReader = new BufferedReader(new FileReader(fileFactory.readFile(marketDataConfigProperties.getInputFile())));
            }
        }
        return priceGroup;
    }

}
