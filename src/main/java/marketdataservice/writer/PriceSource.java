package marketdataservice.writer;

import lombok.extern.log4j.Log4j2;
import marketdataservice.main.DataService;
import marketdataservice.config.AppConfig;
import marketdataservice.model.PriceData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
@Order(value=20)
@Log4j2
public class PriceSource implements Runnable, DataService {

    @Autowired @AppConfig.WriterExecutor
    private ScheduledExecutorService executor;

    @Autowired
    private PriceStreamReader priceStreamReader;

    //another way to add subscriber is exposing a subscribe method which will add caller as a member of subscriber
    @Autowired
    private List<PriceSourceSubscriber> subscribers;

    //in the sample output of this assignment, the data is read starting from second 1, however the data is persisted from
    //second 0, meaning we are persisting some data that does not come from data source, not sure if this is a potential bug.
    //Here we are reading from second 0 to avoid this problem
    @Override
    public void start() {
        executor.scheduleAtFixedRate(this, 0, 1, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        try {
            List<PriceData> nextPriceGroup = priceStreamReader.readNextPriceGroup();
            subscribers.forEach(subscriber -> subscriber.onNewPriceGroup(nextPriceGroup));
        }
        catch (Exception e) {
            log.error("Error reading price stream", e);
        }
    }

}
