package marketdataservice.writer;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class FileFactory {

    public File readFile(String input) throws IOException {
        return new File(input);
    }
}
