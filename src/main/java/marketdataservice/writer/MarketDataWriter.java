package marketdataservice.writer;

import marketdataservice.model.MarketDataModel;
import marketdataservice.model.PriceData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MarketDataWriter implements PriceSourceSubscriber {

    @Autowired
    private MarketDataModel marketDataModel;

    @Override
    public void onNewPriceGroup(List<PriceData> priceGroup) {
        marketDataModel.update(priceGroup);
    }
}
