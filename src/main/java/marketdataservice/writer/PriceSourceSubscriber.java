package marketdataservice.writer;

import marketdataservice.model.PriceData;

import java.util.List;

public interface PriceSourceSubscriber {
    void onNewPriceGroup(List<PriceData> priceGroup);
}
