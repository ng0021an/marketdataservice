package marketdataservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

@Configuration
public class AppConfig {

    @Autowired
    private MarketDataConfigProperties marketDataConfigProperties;

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
        return new HibernateJpaSessionFactoryBean();
    }

    @Bean
    @WriterExecutor
    public ScheduledExecutorService getWriterExecutor() {
        return Executors.newSingleThreadScheduledExecutor(getDaemonThreadFactory());
    }

    @Bean
    @ReaderExecutor
    public ScheduledExecutorService getReaderExecutor() {
        return Executors.newScheduledThreadPool(marketDataConfigProperties.getNumReaderThread());
    }

    @Bean
    @ReportExecutor
    public ScheduledExecutorService getReportExecutor() {
        return Executors.newScheduledThreadPool(marketDataConfigProperties.getNumReportThread());
    }

    //Writer threads should be daemon so that it does not block the exit of JVM
    //Reader threads could possibly be daemon thread, however a bit concerned about half-done persistence
    @Bean
    public ThreadFactory getDaemonThreadFactory() {
        return r -> {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            return t;
        };
    }

    //use annotation instead of string qualifier for compile time detection of error
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Qualifier
    public @interface WriterExecutor {}

    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Qualifier
    public @interface ReaderExecutor {}

    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Qualifier
    public @interface ReportExecutor {}

    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Qualifier
    public @interface PriceSourceSubscriber {}

}
