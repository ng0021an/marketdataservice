package marketdataservice.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix="marketData")
@Getter @Setter
public class MarketDataConfigProperties {

    private String inputFile;
    private List<SymbolConfig> symbolConfigs = new ArrayList<>();
    private int numReaderThread;
    private int numReportThread;
    private int reportGenerationDelayInSeconds;
    private long kHighestPrice;
    private int averagePricePeriodInSeconds;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SymbolConfig {
        private String symbol;
        private int frequencyInSeconds;
    }
}
