Feature: Report Generator
  As a user
  I want to generate the kth highest price over the entire period and the average price for the past n seconds after m seconds

  Scenario: k = 2, n = 10, m = 30
    Given k = 2, n = 10, m = 30, file = input/sample.txt
    When I run the program
    Then I should get
    |symbol|kth_highest_price|average|
    |GOOG  |85.60            |84.5675|
    |BP.L  |1455.30          |1450.625|
    |BT.L  |105.90           |101.96666666666665|
    |VOD.L |92.30            |90.76666666666667|