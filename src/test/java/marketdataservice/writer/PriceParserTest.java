package marketdataservice.writer;

import marketdataservice.model.PriceData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PriceParserTest {

    private final PriceParser priceParser = new PriceParser();
    private final String validData = "BT.L:100";
    private final String invalidData = "BT.L";
    private final ZonedDateTime now = ZonedDateTime.now();
    private final PriceData expectedPriceData = new PriceData(now, "BT.L", new BigDecimal("100"));

    @Test
    public void when_data_string_is_valid_should_return_correct_price_data() {
        assertThat(priceParser.parsePriceData(validData, now), equalTo(expectedPriceData));
    }

    @Test(expected=RuntimeException.class)
    public void when_data_string_does_not_have_enough_info_should_throw_exception() {
        priceParser.parsePriceData(invalidData, now);
    }

}
