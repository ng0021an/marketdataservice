package marketdataservice.repository;

import marketdataservice.config.TestPersistenceConfig;
import marketdataservice.model.PriceData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
@ContextConfiguration(classes={TestPersistenceConfig.class})
@Transactional(propagation= Propagation.NOT_SUPPORTED)
public class PriceDataRepositoryTest {

    @Autowired
    private PriceDataRepository priceDataRepository;

    @Before
    public void init() {
        priceDataRepository.clear();
    }

    @Test
    public void when_data_is_saved_the_database_must_contain_the_saved_data() {
        PriceData priceData = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("200"));
        priceDataRepository.save(priceData);
        List res = priceDataRepository.getAll();
        assertThat(res.size(), equalTo(1));
        assertThat(res.get(0), equalTo(priceData));
    }

    @Test
    public void should_calculate_correct_kth_highest_price() {
        PriceData A1 = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("150"));
        PriceData A2 = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("200"));
        PriceData A3 = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("100"));
        PriceData A4 = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("150"));
        PriceData A5 = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("200"));
        PriceData A6 = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("50"));
        PriceData A7 = new PriceData(ZonedDateTime.now(), "A", new BigDecimal("200"));

        PriceData B1 = new PriceData(ZonedDateTime.now(), "B", new BigDecimal("30"));
        PriceData B2 = new PriceData(ZonedDateTime.now(), "B", new BigDecimal("20"));
        PriceData B3 = new PriceData(ZonedDateTime.now(), "B", new BigDecimal("40"));
        PriceData B4 = new PriceData(ZonedDateTime.now(), "B", new BigDecimal("30"));
        PriceData B5 = new PriceData(ZonedDateTime.now(), "B", new BigDecimal("40"));
        PriceData B6 = new PriceData(ZonedDateTime.now(), "B", new BigDecimal("30"));
        PriceData B7 = new PriceData(ZonedDateTime.now(), "B", new BigDecimal("10"));

        Arrays.asList(A1, A2, A3, A4, A5, A6, A7, B1, B2, B3, B4, B5, B6, B7).forEach(priceData -> priceDataRepository.save(priceData));

        assertThat(priceDataRepository.getKthHighestPriceForSymbol("A", 1).doubleValue(), equalTo(200.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("A", 2).doubleValue(), equalTo(200.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("A", 3).doubleValue(), equalTo(200.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("A", 4).doubleValue(), equalTo(150.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("A", 5).doubleValue(), equalTo(150.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("A", 6).doubleValue(), equalTo(100.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("A", 7).doubleValue(), equalTo(50.0));

        assertThat(priceDataRepository.getKthHighestPriceForSymbol("B", 1).doubleValue(), equalTo(40.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("B", 2).doubleValue(), equalTo(40.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("B", 3).doubleValue(), equalTo(30.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("B", 4).doubleValue(), equalTo(30.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("B", 5).doubleValue(), equalTo(30.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("B", 6).doubleValue(), equalTo(20.0));
        assertThat(priceDataRepository.getKthHighestPriceForSymbol("B", 7).doubleValue(), equalTo(10.0));
    }

    @Test
    public void should_calculate_correct_average_price() {
        ZonedDateTime now = ZonedDateTime.now();
        PriceData A1 = new PriceData(now.minus(30, ChronoUnit.SECONDS), "A", new BigDecimal("50"));
        PriceData A2 = new PriceData(now.minus(25, ChronoUnit.SECONDS), "A", new BigDecimal("100"));
        PriceData A3 = new PriceData(now.minus(20, ChronoUnit.SECONDS), "A", new BigDecimal("150"));
        PriceData A4 = new PriceData(now.minus(15, ChronoUnit.SECONDS), "A", new BigDecimal("200"));
        PriceData A5 = new PriceData(now.minus(10, ChronoUnit.SECONDS), "A", new BigDecimal("140"));
        PriceData A6 = new PriceData(now.minus(5, ChronoUnit.SECONDS), "A", new BigDecimal("120"));
        PriceData A7 = new PriceData(now.minus(0, ChronoUnit.SECONDS), "A", new BigDecimal("100"));

        PriceData B1 = new PriceData(now.minus(30, ChronoUnit.SECONDS), "B", new BigDecimal("10"));
        PriceData B2 = new PriceData(now.minus(27, ChronoUnit.SECONDS), "B", new BigDecimal("30"));
        PriceData B3 = new PriceData(now.minus(24, ChronoUnit.SECONDS), "B", new BigDecimal("20"));
        PriceData B4 = new PriceData(now.minus(21, ChronoUnit.SECONDS), "B", new BigDecimal("50"));
        PriceData B5 = new PriceData(now.minus(18, ChronoUnit.SECONDS), "B", new BigDecimal("60"));
        PriceData B6 = new PriceData(now.minus(15, ChronoUnit.SECONDS), "B", new BigDecimal("40"));
        PriceData B7 = new PriceData(now.minus(12, ChronoUnit.SECONDS), "B", new BigDecimal("70"));
        PriceData B8 = new PriceData(now.minus(9, ChronoUnit.SECONDS), "B", new BigDecimal("80"));
        PriceData B9 = new PriceData(now.minus(6, ChronoUnit.SECONDS), "B", new BigDecimal("20"));
        PriceData B10 = new PriceData(now.minus(3, ChronoUnit.SECONDS), "B", new BigDecimal("90"));
        PriceData B11 = new PriceData(now.minus(0, ChronoUnit.SECONDS), "B", new BigDecimal("40"));

        Arrays.asList(A1, A2, A3, A4, A5, A6, A7, B1, B2, B3, B4, B5, B6, B7, B8, B9, B10, B11).forEach(priceData -> priceDataRepository.save(priceData));

        assertThat(priceDataRepository.getAveragePriceForSymbol("A", 1), equalTo(100.0));
        assertThat(priceDataRepository.getAveragePriceForSymbol("A", 6), equalTo(110.0));
        assertThat(priceDataRepository.getAveragePriceForSymbol("A", 10), equalTo(120.0));
        assertThat(priceDataRepository.getAveragePriceForSymbol("A", 15), equalTo(140.0));
        assertThat(priceDataRepository.getAveragePriceForSymbol("A", 21), equalTo(142.0));
        assertThat(priceDataRepository.getAveragePriceForSymbol("A", 28), equalTo(135.0));
        assertThat(priceDataRepository.getAveragePriceForSymbol("A", 30), equalTo(122.85714285714286));

    }

}
