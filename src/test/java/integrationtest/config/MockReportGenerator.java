package integrationtest.config;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import marketdataservice.config.MarketDataConfigProperties;
import marketdataservice.report.ReportGenerator;
import marketdataservice.report.ReportGeneratorQueryPlan;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;

//Default implementation will output to console/file and terminate, need to switch to this mock to save the output for assertion
@Log4j2
public class MockReportGenerator extends ReportGenerator {

    @Getter private CompletableFuture<List<ReportGeneratorQueryPlan.ReportSymbolData>> reportDataFuture;

    public MockReportGenerator(ScheduledExecutorService executorService, MarketDataConfigProperties marketDataConfigProperties, ReportGeneratorQueryPlan reportGeneratorQueryPlan) {
        this.executorService = executorService;
        this.marketDataConfigProperties = marketDataConfigProperties;
        this.reportGeneratorQueryPlan = reportGeneratorQueryPlan;
        this.reportDataFuture = new CompletableFuture<>();
    }

    @Override
    public void run() {
        try {
            reportDataFuture.complete(reportGeneratorQueryPlan.generateReportData());
        }
        catch (Exception e) {
            log.error("Error generating report", e);
        }
    }
}
