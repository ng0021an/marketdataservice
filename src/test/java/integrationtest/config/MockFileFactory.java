package integrationtest.config;

import lombok.RequiredArgsConstructor;
import marketdataservice.writer.FileFactory;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.IOException;

//The default file factory will read from external file, need to switch to this mock to read from bundled file
@RequiredArgsConstructor
public class MockFileFactory extends FileFactory {

    private final ResourceLoader resourceLoader;

    public File readFile(String input) throws IOException {
        return resourceLoader.getResource("classpath:" + input).getFile();
    }
}
