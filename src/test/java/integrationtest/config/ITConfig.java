package integrationtest.config;

import marketdataservice.config.AppConfig;
import marketdataservice.config.MarketDataConfigProperties;
import marketdataservice.main.DataService;
import marketdataservice.main.MarketDataServiceRunner;
import marketdataservice.report.ReportGenerator;
import marketdataservice.report.ReportGeneratorQueryPlan;
import marketdataservice.writer.FileFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

@TestConfiguration
@EntityScan(basePackages={"marketdataservice.model"})
@SpringBootApplication(scanBasePackages={"marketdataservice"})
public class ITConfig {

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    @AppConfig.ReportExecutor
    private ScheduledExecutorService executorService;

    @Autowired
    private ReportGeneratorQueryPlan reportGeneratorQueryPlan;

    @Autowired
    private List<DataService> dataServices;

    @Bean
    public FileFactory fileFactory() {
        return new MockFileFactory(resourceLoader);
    }

    @Bean
    public ReportGenerator reportGenerator() {
        return new MockReportGenerator(executorService, marketDataConfigProperties, reportGeneratorQueryPlan);
    }

    @MockBean
    private MarketDataConfigProperties marketDataConfigProperties;

    @MockBean
    private MarketDataServiceRunner marketDataServiceRunner;

}
