package integrationtest.steps;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import integrationtest.config.ITConfig;
import integrationtest.config.MockReportGenerator;
import marketdataservice.config.AppConfig;
import marketdataservice.config.MarketDataConfigProperties;
import marketdataservice.main.DataService;
import marketdataservice.report.ReportGenerator;
import marketdataservice.report.ReportGeneratorQueryPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes={AppConfig.class, ITConfig.class})
@TestPropertySource("/test.properties")
public class MarketDataServiceSteps {

    @Autowired
    private MarketDataConfigProperties marketDataConfigProperties;

    @Autowired
    private List<DataService> dataServices;

    @Autowired
    private ReportGenerator mockReportGenerator;

    @Before
    public void init() {
        doReturn(4).when(marketDataConfigProperties).getNumReaderThread();
        doReturn(3).when(marketDataConfigProperties).getNumReportThread();
        when(marketDataConfigProperties.getSymbolConfigs()).thenReturn(
                Arrays.asList(
                        new MarketDataConfigProperties.SymbolConfig("GOOG", 3),
                        new MarketDataConfigProperties.SymbolConfig("BP.L", 3),
                        new MarketDataConfigProperties.SymbolConfig("BT.L", 5),
                        new MarketDataConfigProperties.SymbolConfig("VOD.L", 5)
                )
        );
    }

    @Given("^k = (.*), n = (.*), m = (.*), file = (.*)$")
    public void kth_highest_price_and_average_period_in_seconds_and_report_generation_delay_in_seconds(long kthHighestPrice, int averagePeriodInSeconds, int reportGenerationDelayInSeconds, String file) {
        doReturn(kthHighestPrice).when(marketDataConfigProperties).getKHighestPrice();
        doReturn(averagePeriodInSeconds).when(marketDataConfigProperties).getAveragePricePeriodInSeconds();
        doReturn(reportGenerationDelayInSeconds).when(marketDataConfigProperties).getReportGenerationDelayInSeconds();
        doReturn(file).when(marketDataConfigProperties).getInputFile();
    }

    @When("^I run the program$")
    public void i_run_the_program() {
        dataServices.forEach(dataService -> dataService.start());
    }

    @Then("^I should get$")
    public void i_should_get(DataTable dataTable) throws InterruptedException, ExecutionException, TimeoutException {
        List<ReportGeneratorQueryPlan.ReportSymbolData> reportData = ((MockReportGenerator)mockReportGenerator).getReportDataFuture().get(100, TimeUnit.SECONDS);
        List<ReportGeneratorQueryPlan.ReportSymbolData> expectedData = dataTable.asMaps(String.class,String.class).stream().map(m -> new ReportGeneratorQueryPlan.ReportSymbolData(
                m.get("symbol"),
                new BigDecimal(m.get("kth_highest_price")),
                Double.valueOf(m.get("average"))
        )).collect(Collectors.toList());
        assertThat(reportData, equalTo(expectedData));
    }
}
