package integrationtest.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = {"pretty", "html:target/cucumber"},
        features = "src/test/resources",
        glue = "integrationtest.steps")
public class CucumberTest {
}
