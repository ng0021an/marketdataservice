**CONTENT:
	- market-data-service-0.1.0.jar - the main jar
	- application.yml - a sample application properties file
	- marketdataservice.txt - a sample output file
	- marketdataservice.zip - the zipped source code
	- README.txt - documentation (this file)

**HOW TO RUN:
	- To run the main jar:
	java -jar market-data-service-0.1.0.jar (This assumes a SampleData.txt input file in the same folder with the jar)
    	or 
	java -jar market-data-service-0.1.0.jar —spring.config.location={path_to_application_yml_file} (e.g. java -jar market-data-service-0.1.0.jar —spring.config.location=application.yml)

	- A sample application.yml file:
===================================BEGIN OF FILE======================
market-data:
  input-file: SampleData.txt
  num-reader-thread: 4
  num-report-thread: 3
  report-generation-delay-in-seconds: 30
  k-highest-price: 2
  average-price-period-in-seconds: 10
  symbol-configs:
    - symbol: GOOG
      frequency-in-seconds: 3
    - symbol: BP.L
      frequency-in-seconds: 3
    - symbol: BT.L
      frequency-in-seconds: 5
    - symbol: VOD.L
      frequency-in-seconds: 5

springjpa:
  properties.hibernate.current_session_context_class: org.springframework.orm.hibernate4.SpringSessionContext

logging:
  level:
    org.springframework: ERROR
    marketdataservice: INFO
================================END OF FILE============================

	- to run integration test
	mvn integration-test
	- Note that this program requires java 8.
	- Output will be on console and also in file marketdataservice.txt

**ASSUMPTIONS:
	- Time format in the log is different from the sample output, we log HH:mm:ss instead of second.
	- Average price is rounded to 2 decimal places.
	- Price read start from second 0 instead of 1 as in sample output. price persistance will start after price read.
	- Average will include the price at the start of the 10 second period.
The 2nd highest price will be the 2nd highest entry instead of value. For example if the price is 5, 5, 5, 4, 3, 2, the 2nd highest price will be 5 instead of 4. The method used here is for each entry A, count the number of entries strictly greater than A (called m) and number of entries greater than or equal to A (called n). Iif (m < k <= n) then that entry will be the kth largest value. Note that there will be multiple of such entries, all have the same price so we could pick any one and get the price.

**ADDITIONAL DEPENDENCIES:
	- Lombok: to reduce boiler plate code, for intellij need to install lombok plugin and enable annotation processing.
	- Cucumber: for writing BDD test